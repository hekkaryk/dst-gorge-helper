﻿using System;
using System.Linq;
using System.Text;
using static DST_TheGorgeEventHelper.Enums;

namespace DST_TheGorgeEventHelper
{
    class Whole
    {
        public Part[] Parts { get; set; }

        public Whole(params Part[] xParts)
        {
            Parts = xParts;
        }

        public override string ToString()
        {
            StringBuilder rewards = new StringBuilder();
            int partsCount = Parts.Count();
            for (int i = 0; i < partsCount; ++i)
            {
                rewards.Append(Parts[i].ToString());

                if (i < partsCount - 1)
                    rewards.Append(", ");
            }

            return rewards.ToString();
        }

        public void Show()
        {
            Console.Write("\t");
            int partsCount = Parts.Count();
            for (int i = 0; i < partsCount; ++i)
            {
                Parts[i].Show();

                if (i < partsCount - 1)
                    Console.Write(", ");
            }
        }
    }

    class Recipe : Whole
    {
        public Recipe(params object[] xParts) : base()
        {
            Parts = new Part[xParts.Length];

            for (int i = 0; i < xParts.Length; ++i)
            {
                if (xParts[i] is Part)
                    Parts[i] = (Part)xParts[i];
                else if (xParts[i] is Ingredient)
                    Parts[i] = new Part((Ingredient)xParts[i]);
            }
        }

        internal bool Contains(string name)
        {
            bool result = false;
            foreach (var part in Parts)
            {
                if (part.Name.ToLowerInvariant().StartsWith(name.ToLowerInvariant()) ||
                    EnumNameToName(part.Name).ToLowerInvariant().StartsWith(name.ToLowerInvariant()) ||
                    EnumNameToName(part.Name).ToLowerInvariant().Contains(name.ToLowerInvariant()))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}
