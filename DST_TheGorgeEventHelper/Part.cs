﻿using System;
using static DST_TheGorgeEventHelper.Enums;
using static DST_TheGorgeEventHelper.TheGorge;

namespace DST_TheGorgeEventHelper
{
    class Part
    {
        internal string Name { get; set; }
        internal int Quantity { get; set; }

        public void Init(string xName, int xQuantity)
        {
            Name = xName;
            Quantity = xQuantity;
        }

        public Part(string xName, int xQuantity = 1)
        {
            Init(xName, xQuantity);
        }

        public Part(Ingredient xIngredientName, int xQuantity = 1)
        {
            Init(xIngredientName.ToString(), xQuantity);
        }

        public Part(Treasure xRewardName, int xQuantity = 1)
        {
            Init(xRewardName.ToString(), xQuantity);
        }

        public override string ToString()
        {
            string result = Quantity > 1
                ? string.Format("{0}x {1}", Quantity, EnumNameToName(Name))
                : EnumNameToName(Name);

            return result;
        }

        public void Show()
        {
            string result = Quantity > 1
                ? string.Format("{0}x {1}", Quantity, EnumNameToName(Name))
                : EnumNameToName(Name);

            if (Name.Equals(Treasure.Gnaw__s_Favor.ToString()))
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(result);
                Console.ForegroundColor = consoleForegroundColor;
                Console.BackgroundColor = consoleBackgroundColor;
            }
            else if (Name.Equals(Treasure.Sapphire_Medalion.ToString()))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(result);
                Console.ForegroundColor = consoleForegroundColor;
            }
            else if (Name.Equals(Treasure.Red_Mark.ToString()))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(result);
                Console.ForegroundColor = consoleForegroundColor;
            }
            else
            {
                Console.Write(result);
            }
        }

        public int GetScore()
        {
            int result = 0;

            if (Name.Equals(Treasure.Gnaw__s_Favor.ToString()))
            {
                result = 10000 * Quantity;
            }
            else if (Name.Equals(Treasure.Sapphire_Medalion.ToString()))
            {
                result = 50 * Quantity;
            }
            else if (Name.Equals(Treasure.Red_Mark.ToString()))
            {
                result = 100 * Quantity;
            }
            else
            {
                result = Quantity;
            }

            return result;
        }
    }
}
