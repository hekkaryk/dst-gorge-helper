﻿using System;
using static DST_TheGorgeEventHelper.Enums;

namespace DST_TheGorgeEventHelper
{
    class Variant : IComparable
    {
        internal Crawing[] Crawing { get; set; }
        internal Reward Rewards { get; set; }
        internal Reward SilverPlateRewards { get; set; }

        private void Init(Crawing[] Crawing, Reward Rewards, Reward SilverPlateRewards)
        {
            this.Crawing = Crawing;
            this.Rewards = Rewards;
            this.SilverPlateRewards = SilverPlateRewards;
        }

        public Variant(Crawing xCrawing, Reward xRewards, Reward xSilverPlateRewards)
        {
            Init(new Crawing[] { xCrawing }, xRewards, xSilverPlateRewards);
        }

        public Variant(Crawing xCrawingA, Crawing xCrawingB, Reward xRewards, Reward xSilverPlateRewards)
        {
            Init(new Crawing[] { xCrawingA, xCrawingB }, xRewards, xSilverPlateRewards);
        }

        public Variant(Crawing xCrawingA, Crawing xCrawingB, Crawing xCrawingC, Reward xRewards, Reward xSilverPlateRewards)
        {
            Init(new Crawing[] { xCrawingA, xCrawingB, xCrawingC }, xRewards, xSilverPlateRewards);
        }

        public Variant(Crawing xCrawingA, Crawing xCrawingB, Crawing xCrawingC, Crawing xCrawingD, Reward xRewards, Reward xSilverPlateRewards)
        {
            Init(new Crawing[] { xCrawingA, xCrawingB, xCrawingC, xCrawingD }, xRewards, xSilverPlateRewards);
        }

        public void Show()
        {
            Console.Write("Standard reward:{0}\t", Environment.NewLine);
            Rewards.Show();
            Console.Write("{0}Silver plate reward:{0}\t", Environment.NewLine);
            SilverPlateRewards.Show();
            Console.WriteLine();
        }

        public int GetValue()
        {
            int scoreThis = 0;

            foreach (var part in this.Rewards.Parts)
            {
                scoreThis += part.GetScore() * 10;
            }
            foreach (var part in this.SilverPlateRewards.Parts)
            {
                scoreThis += part.GetScore();
            }

            return scoreThis;
        }

        public int CompareTo(object other)
        {
            int scoreThis = 0;
            int scoreOther = 0;

            foreach (var part in this.Rewards.Parts)
            {
                scoreThis += part.GetScore() * 10;
            }
            foreach (var part in this.SilverPlateRewards.Parts)
            {
                scoreThis += part.GetScore();
            }

            foreach (var part in ((Variant)other).Rewards.Parts)
            {
                scoreOther += part.GetScore() * 10;
            }
            foreach (var part in ((Variant)other).SilverPlateRewards.Parts)
            {
                scoreOther += part.GetScore();
            }

            return scoreOther - scoreThis;
        }

        //public Variant(Crawing crawing, Reward rewards, Reward silverPlateRewards)
        //{
        //    this.Crawing = new Crawing[] { crawing };
        //    this.Rewards = rewards;
        //    this.SilverPlateRewards = silverPlateRewards;
        //}

        //public Variant(Crawing[] crawing, Reward rewards, Reward silverPlateRewards)
        //{
        //    this.Crawing = crawing;
        //    this.Rewards = rewards;
        //    this.SilverPlateRewards = silverPlateRewards;
        //}
    }
}
