﻿using System;
using System.Runtime.InteropServices;
using static DST_TheGorgeEventHelper.Enums;

namespace DST_TheGorgeEventHelper
{
    internal static class WindowsMagic
    {
        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

#pragma warning disable IDE0044 // Add readonly modifier // Don't tell me how to life!
        private static IntPtr ThisConsole = GetConsoleWindow();
#pragma warning restore IDE0044 // Add readonly modifier // I like to life dangerously and don't use readonly!

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        internal static void MaximizeWindow()
        {
            ShowWindow(ThisConsole, (int)WINDOW_ACTION.MAXIMIZE);
        }
    }
}
