﻿namespace DST_TheGorgeEventHelper
{
    internal static class Enums
    {
        internal static string EnumNameToName(string enumName)
        {
            return enumName.Replace("__", "'").Replace('_', ' ');
        }

        internal static string NameToEnumName(string name)
        {
            return name.Replace("'", "__").Replace(' ', '_');
        }


        internal enum Crawing
        {
            Undefined,
            None,
            Any,
            Bread,
            Cheese,
            Dessert,
            Fish,
            Meat,
            Snack,
            Pasta,
            Soup,
            Veggie
        }

        internal enum DishName
        {
            Bread,
            Chips,
            Veggie_Soup,
            Jelly_Sandwich,
            Fish_Stew,
            Turnip_Cake,
            Potato_Pancakes,
            Potato_Soup,
            Fishball_Skewers,
            Meatballs,
            Meat_Skewers,
            Stone_Soup,
            Croquette,
            Roasted_Veggies,
            Meatloaf,
            Carrot_Soup,
            Fish_Pie,
            Fish_And_Chips,
            Meat_Pie,
            Slider,
            Jam,
            Jelly_Roll,
            Carrot_Cake,
            Mashed_Potatoes,
            Garlic_Bread,
            Tomato_Soup,
            Sausage,
            Candied_Fish,
            Stuffed_Mushroom,
            Ratatouille,
            Bruschetta,
            Meat_Stew,
            Hamburger,
            Fish_Burger,
            Mushroom_Burger,
            Fish_Steak,
            Curry,
            Spaghetti_And_Meatballs,
            Lasagna,
            Poached_Fish,
            Shepherds_Pie,
            Candy,
            Pudding,
            Waffles,
            Berry_Tart,
            Mac__Ncheese,
            Bagel__Nfish,
            Grilled_Cheese,
            Cream_Of_Mushroom,
            Pierogies,
            Manicotti,
            Cheeseburger,
            Fettuccine,
            Onion_Soup,
            Breaded_Cutlet,
            Creamy_Fish,
            Pizza,
            Pot_Roast,
            Crab_Cake,
            Steak_Frites,
            Shooter_Sandwich,
            Bacon_Wrapped_Meat,
            Crab_Roll,
            Meat_Wellington,
            Crab_Ravioli,
            Caramel_Cube,
            Scone,
            Trifle,
            Cheesecake,
            Quagmire_Syrup
        }

        internal enum Ingredient
        {
            Flour,
            Wheat,
            Potato,
            Spot_Spice,
            Mushroom,
            Carrot,
            Turnip,
            Berries,
            Syrup,
            Salmon,
            Foliage,
            Onion,
            Twigs,
            Meat_Scraps,
            Garlic,
            Rocks,
            Toma_Root,
            Goat_Milk,
            Meat,
            Crab_Meat,
            Sap
        }

        internal enum Source
        {
            Cooking,
            Farming,
            Berry_Trees,
            Salt_Pond,
            Mumsy,
            Sammy,
            Beefalos,
            Rabbits,
            Pigeons,
            Rock_Brab,
        }

        internal enum CraftingStation
        {
            Cookpot,
            Grill,
            Oven,
            Millstone,
            Tree_Sapping_Kit
        }

        internal enum Treasure
        {
            Old_Coin,
            Sapphire_Medalion,
            Red_Mark,
            Gnaw__s_Favor
        }

        internal enum WINDOW_ACTION
        {
            HIDE = 0,
            MAXIMIZE = 3,
            MINIMIZE = 6,
            RESTORE = 9
        }
    }
}
