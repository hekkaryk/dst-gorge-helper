﻿using System;
using System.Collections.Generic;
using System.Linq;
using static DST_TheGorgeEventHelper.Enums;
using static DST_TheGorgeEventHelper.Defaults;

#pragma warning disable IDE0044 // Add readonly modifier // Suppresses idiotic "Add readonly modifier" for all fields
namespace DST_TheGorgeEventHelper
{
    internal static class TheGorge
    {
        internal static Crawing currentCrawing = Crawing.Undefined;
        static Dictionary<CraftingStation, bool> availableCraftingStations = new Dictionary<CraftingStation, bool>()
        {
            { CraftingStation.Cookpot, false },
            { CraftingStation.Grill, false },
            { CraftingStation.Oven, false }
        };

        internal static ConsoleColor consoleBackgroundColor = Console.BackgroundColor;
        internal static ConsoleColor consoleForegroundColor = Console.ForegroundColor;

        private static void WriteRed(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = consoleForegroundColor;
        }

        private static void WriteGreen(string text)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ForegroundColor = consoleForegroundColor;
        }


        internal static void DoTheThing()
        {
            bool loopMenu = true;
            bool loopCrawing = true;
            Console.WriteLine("Welcome in The Gorge Event Recipe Helper by Hekkaryk! ^_^");
            while (loopMenu)
            {
                Console.WriteLine("[1][c] Browse recipes by crawing (pick this if you are playing, fastest suggestions!)");
                Console.WriteLine("[2][i] Browse recipes by ingredient");
                Console.WriteLine("[3][r] Browse recipes by name");
                Console.WriteLine("[4][a] Browse recipes by crafting station");

                if (availableCraftingStations[CraftingStation.Cookpot])
                    WriteGreen("[5][o] You have Cookpot");
                else
                    WriteRed("[5][o] No Cookpot");

                if (availableCraftingStations[CraftingStation.Grill])
                    WriteGreen("[6][g] You have Grill");
                else
                    WriteRed("[6][g] No Grill");

                if (availableCraftingStations[CraftingStation.Oven])
                    WriteGreen("[7][v] You have Oven");
                else
                    WriteRed("[7][v] No Oven");


                Console.WriteLine("[q][Esc] Exit app");

                ConsoleKeyInfo cki = Console.ReadKey();
                Console.WriteLine();
                switch (cki.Key)
                {
                    case ConsoleKey.Escape:
                    case ConsoleKey.Q:
                        loopMenu = false;
                        break;
                    case ConsoleKey.D1:
                    case ConsoleKey.C:
                        ByCrawing();
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.I:
                        ByIngredient();
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.R:
                        ByDishName();
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.A:
                        ByOwnedCraftingStations();
                        break;
                    case ConsoleKey.D5:
                    case ConsoleKey.O:
                        availableCraftingStations[CraftingStation.Cookpot] = !availableCraftingStations[CraftingStation.Cookpot];
                        break;
                    case ConsoleKey.D6:
                    case ConsoleKey.G:
                        availableCraftingStations[CraftingStation.Grill] = !availableCraftingStations[CraftingStation.Grill];
                        break;
                    case ConsoleKey.D7:
                    case ConsoleKey.V:
                        availableCraftingStations[CraftingStation.Oven] = !availableCraftingStations[CraftingStation.Oven];
                        break;
                    default:
                        Console.WriteLine("You pressed button without option attached to it.");

                        Console.WriteLine();
                        Console.WriteLine("Press any key to return to main screen.");
                        Console.ReadKey();
                        break;
                }
                Console.Clear();
            }
        }

        private static void ByCrawing()
        {
            bool loopCrawing = true;
            while (loopCrawing)
            {
                Console.Clear();
                Console.WriteLine("Select crawing:");
                Console.WriteLine("[1][B] Bread");
                Console.WriteLine("[2][C] Cheese");
                Console.WriteLine("[3][D] Dessert");
                Console.WriteLine("[4][F] Fish");
                Console.WriteLine("[5][M] Meat");
                Console.WriteLine("[6][N] Snack");
                Console.WriteLine("[7][P] Pasta");
                Console.WriteLine("[8][S] Soup");
                Console.WriteLine("[9][V] Veggie");
                Console.WriteLine("[x][Backspace] To go back");


                ConsoleKeyInfo cki = Console.ReadKey();
                Console.WriteLine();
                Console.Clear();

                switch (cki.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.B:
                        currentCrawing = Crawing.Bread;
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.C:
                        currentCrawing = Crawing.Cheese;
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.D:
                        currentCrawing = Crawing.Dessert;
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.F:
                        currentCrawing = Crawing.Fish;
                        break;
                    case ConsoleKey.D5:
                    case ConsoleKey.M:
                        currentCrawing = Crawing.Meat;
                        break;
                    case ConsoleKey.D6:
                    case ConsoleKey.N:
                        currentCrawing = Crawing.Snack;
                        break;
                    case ConsoleKey.D7:
                    case ConsoleKey.P:
                        currentCrawing = Crawing.Pasta;
                        break;
                    case ConsoleKey.D8:
                    case ConsoleKey.S:
                        currentCrawing = Crawing.Soup;
                        break;
                    case ConsoleKey.D9:
                    case ConsoleKey.V:
                        currentCrawing = Crawing.Veggie;
                        break;
                    case ConsoleKey.X:
                    case ConsoleKey.Backspace:
                        loopCrawing = false;
                        break;
                    default:
                        Console.WriteLine("Unknown crawing!");
                        currentCrawing = Crawing.Undefined;
                        break;
                }

                if (loopCrawing)
                {
                    foreach (Dish dish in DefaultRecipeList.Where(m1z => m1z.CraftingStation.Any(m1v => availableCraftingStations[m1v]) && m1z.Variants.Any(m1x => m1x.Crawing.Contains(currentCrawing))).OrderBy(m1c => m1c.GetScore()))
                    {
                        dish.Show();
                    }

                    Console.WriteLine();
                    Console.WriteLine("Press any key to select new crawing.");
                    Console.ReadKey();
                }
            }
        }

        private static void ByIngredient()
        {
            currentCrawing = Crawing.Any;
            Console.WriteLine("Enter part of ingredient name:");
            string name = Console.ReadLine();

            foreach (Dish dish2 in DefaultRecipeList.OrderBy(m2x => m2x.GetScore()))//Descending(m2x => m2x.GetScore()))
            {
                if (dish2.RecipesContains(name))
                    dish2.Show();
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to return to main screen.");
            Console.ReadKey();
        }

        private static void ByDishName()
        {
            currentCrawing = Crawing.Any;
            Console.WriteLine("Enter part of dish name:");
            string dishName = Console.ReadLine();

            foreach (Dish dish3 in DefaultRecipeList.OrderBy(m2x => m2x.GetScore()))
            {
                if (EnumNameToName(dish3.Name.ToString()).ToLowerInvariant().Contains(dishName.ToLowerInvariant()))
                    dish3.Show();
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to return to main screen.");
            Console.ReadKey();
        }

        private static void ByOwnedCraftingStations()
        {
            currentCrawing = Crawing.Any;
            Console.WriteLine("Dishes available for owned crafting stations:");

            foreach (Dish dish4 in DefaultRecipeList.Where(m4z => m4z.CraftingStation.Any(m4c => availableCraftingStations[m4c])).OrderBy(m4x => m4x.GetScore()))
            {
                dish4.Show();
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to return to main screen.");
            Console.ReadKey();
        }
    }
}
#pragma warning restore IDE0044 // Add readonly modifier