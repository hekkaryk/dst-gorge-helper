﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DST_TheGorgeEventHelper.Enums;

namespace DST_TheGorgeEventHelper
{
    class Reward : Whole, IComparable
    {
        public Reward(params object[] xParts) : base()
        {
            Parts = new Part[xParts.Length];

            for (int i = 0; i < xParts.Length; ++i)
            {
                if (xParts[i] is Part)
                    Parts[i] = (Part)xParts[i];
                else if (xParts[i] is Treasure)
                    Parts[i] = new Part((Treasure)xParts[i]);
                else
                    throw new Exception("You goofed up reward definition.");
            }
        }

        public int GetValue()
        {
            int score = 0;

            foreach (var part in this.Parts)
            {
                score += part.GetScore();
            }

            return score;
        }

        public int CompareTo(object other)
        {
            int scoreThis = 0;
            int scoreOther = 0;

            foreach (var part in this.Parts)
            {
                scoreThis += part.GetScore();
            }

            foreach (var part in ((Reward)other).Parts)
            {
                scoreOther += part.GetScore();
            }

            return scoreOther - scoreThis;
        }
    }
}
