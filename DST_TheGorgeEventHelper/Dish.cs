﻿using System;
using System.Linq;
using System.Text;
using static DST_TheGorgeEventHelper.Enums;
using static DST_TheGorgeEventHelper.TheGorge;

namespace DST_TheGorgeEventHelper
{
    class Dish : IComparable
    {
        internal int Number { get; set; }
        internal DishName Name { get; set; }
        internal CraftingStation[] CraftingStation { get; set; }
        internal Variant[] Variants { get; set; }
        internal Recipe[] Recipes { get; set; }

        public Dish(int Number, DishName Name, CraftingStation CraftingStation, Variant Variant, params Recipe[] Recipes)
        {
            this.Number = Number;
            this.Name = Name;
            this.CraftingStation = new CraftingStation[] { CraftingStation };
            this.Variants = new Variant[] { Variant };
            this.Recipes = Recipes;
        }

        public Dish(int Number, DishName Name, CraftingStation CraftingStationA, CraftingStation CraftingStationB, Variant Variant, params Recipe[] Recipes)
        {
            this.Number = Number;
            this.Name = Name;
            this.CraftingStation = new CraftingStation[] { CraftingStationA, CraftingStationB };
            this.Variants = new Variant[] { Variant };
            this.Recipes = Recipes;
        }

        public Dish(int Number, DishName Name, CraftingStation CraftingStation, Variant VariantA, Variant VariantB, params Recipe[] Recipes)
        {
            this.Number = Number;
            this.Name = Name;
            this.CraftingStation = new CraftingStation[] { CraftingStation };
            this.Variants = new Variant[] { VariantA, VariantB };
            this.Recipes = Recipes;
        }

        public Dish(int Number, DishName Name, CraftingStation CraftingStationA, CraftingStation CraftingStationB, Variant VariantA, Variant VariantB, params Recipe[] Recipes)
        {
            this.Number = Number;
            this.Name = Name;
            this.CraftingStation = new CraftingStation[] { CraftingStationA, CraftingStationB };
            this.Variants = new Variant[] { VariantA, VariantB };
            this.Recipes = Recipes;
        }

        public override string ToString()
        {
            StringBuilder recipes = new StringBuilder();
            foreach (var recipe in Recipes)
            {
                recipes.AppendLine("\t" + recipe.ToString());
            }

            StringBuilder variants = new StringBuilder();
            foreach (var variant in Variants)
            {
                variants.AppendLine(variant.ToString());
            }

            return string.Format("{0}[{1}] {2}; crafted at: {3}{0}Recipe(s):{0}{4}Variants:{0}\t{5}{0}",
                Environment.NewLine, Number, EnumNameToName(Name.ToString()), EnumNameToName(CraftingStation.ToString()), recipes, variants.ToString());
        }

        public void Show()
        {
            var matches = GetVariants();

            if (matches.Count() > 0)
            {
                StringBuilder craftingStations = new StringBuilder();
                for (int i = 0; i < CraftingStation.Count(); ++i)
                {
                    if (i.Equals(0))
                        craftingStations.Append(EnumNameToName(CraftingStation[i].ToString()));
                    else
                        craftingStations.Append(", " + EnumNameToName(CraftingStation[i].ToString()));
                }

                Console.WriteLine();
                Console.WriteLine(string.Format("{0}[{1}] {2}; crafted at: {3}{0}Recipe(s):", Environment.NewLine, Number, EnumNameToName(Name.ToString()), craftingStations));
                foreach (var recipe in Recipes)
                {
                    recipe.Show();
                    Console.WriteLine();
                }

                foreach (var match in matches)
                {
                    match.Show();
                }
            }
        }

        public bool RecipesContains(string name)
        {
            bool result = false;

            foreach (var recipe in Recipes)
            {
                if (recipe.Contains(name))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public int GetScore()
        {
            int score = 0;

            foreach (var variant in Variants.Where(m => m.Crawing.Any(x => x.Equals(currentCrawing))))
            {
                int temp = 0;
                for (int i = 0; i < variant.Rewards.Parts.Count(); ++i)
                {
                    temp += variant.Rewards.Parts[i].GetScore() * 10; // if there is no need for silver plate then it's better than when it's a must
                }

                for (int i = 0; i < variant.SilverPlateRewards.Parts.Count(); ++i)
                {
                    temp += variant.SilverPlateRewards.Parts[i].GetScore();
                }

                if (temp > score)
                    score = temp;
            }

            return score;
        }

        public int CompareTo(object other)
        {
            int bestScoreThis = 0;
            int bestScoreOther = 0;

            foreach (var variant in Variants.Where(m => m.Crawing.Any(x => x.Equals(currentCrawing))))
            {
                int temp = 0;
                for (int i = 0; i < variant.Rewards.Parts.Count(); ++i)
                {
                    temp += variant.Rewards.Parts[i].GetScore() * 10; // if there is no need for silver plate then it's better than when it's a must
                }

                for (int i = 0; i < variant.SilverPlateRewards.Parts.Count(); ++i)
                {
                    temp += variant.Rewards.Parts[i].GetScore();
                }

                if (temp > bestScoreThis)
                    bestScoreThis = temp;
            }

            foreach (var variant in ((Dish)other).Variants.Where(m => m.Crawing.Any(x => x.Equals(currentCrawing))))
            {
                int temp = 0;
                for (int i = 0; i < variant.Rewards.Parts.Count(); ++i)
                {
                    temp += variant.Rewards.Parts[i].GetScore() * 10; // if there is no need for silver plate then it's better than when it's a must
                }

                for (int i = 0; i < variant.SilverPlateRewards.Parts.Count(); ++i)
                {
                    temp += variant.Rewards.Parts[i].GetScore();
                }

                if (temp > bestScoreOther)
                    bestScoreOther = temp;
            }

            return bestScoreOther - bestScoreThis;
        }

        private Variant[] GetVariants()
        {
            return currentCrawing.Equals(Crawing.Any)
                ? Variants
                : Variants.Where(z => z.Crawing.Any(x => x.Equals(currentCrawing))).ToArray();
        }
    }
}
